package archonbp.geocoder.consumer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import archon.bppc.consumer.MidConsumer;
import archon.bppc.model.BpObject;
import archonbp.geocoder.model.ArProperties;
import archonbp.geocoder.model.BaiduJSON;

/**
 * 获取输入队列的数据，处理之后，放到输出队列。
 * Version 1.1: 优化：一分钟配额超限不会退出，而是sleep一分钟之后继续执行。
 * Version 1.2: 需求更新：无经纬度数据的值，不写出。
 * @author AlexGumHub 20160825
 *
 */
public class BaiduGeocoderV2Consumer extends MidConsumer<String, String>{
	
	/**
	 * log4j 2.
	 */
	private static final Logger LOGGER = LogManager.getLogger();
	
	/**
	 * 请求成功的状态码。
	 */
	private static final int SUCCESS = 0;
	
	/**
	 * 请求成功但没有数据的状态码。
	 */
	private static final int SUCCESS_BUT_NO_RESULT = 1;
	
	/**
	 * 请求参数错误的状态码。
	 */
	private static final int PARAMETER_ERROR = 2;
	
	/**
	 * APP不存在，AK有误请检查再重试。
	 * key不存在。
	 */
//	private static final int KEY_NOT_EXIST = 200;
	
	/**
	 * APP IP校验失败。
	 * 本机的IP地址不在白名单内。
	 */
//	private static final int KEY_WHITE_LIST_BAN = 210;
	
	/**
	 * APP Referer校验失败。
	 * key不能用。
	 */
//	private static final int KEY_AUTHORITY_FAILED = 220;
	
	/**
	 * APP 服务被禁用。
	 * key不支持Geocoding v2。
	 */
//	private static final int KEY_SERVICE_BAN = 240;
	
	/**
	 * key的天配额超限的状态码。
	 */
	private static final int OUT_OF_QUOTA_DAY = 302;
	
	/**
	 * key的分钟配额超限的状态码。
	 */
	private static final int OUT_OF_QUOTA_MINUTE = 401;
	
	
//	private static final int NO_STATUS = -1;

	/**
	 * 配置文件。
	 */
	private ArProperties arProperties;
	
	/**
	 * 保存keys。
	 */
	private ArrayList<String> keyList;
	
	/**
	 * 防止一次运行太多数据，令下次运行时读取的数据太多。
	 */
	private static final int Max = 5000000;
	private static int count = 0;
	
	/**
	 * 
	 * @param arProperties 配置文件
	 */
	public BaiduGeocoderV2Consumer(ArProperties arProperties) {
		this.arProperties = arProperties;
		this.keyList = new ArrayList<String>();
		for (String keyString : arProperties.getBaiduKey().split(",")) {
			keyList.add(keyString);
		}
	}

	@Override
	protected void handle(BpObject<String> bpIn, LinkedBlockingQueue<BpObject<String>> outQueue) {
		try {
			// 请求百度地图API直到成功返回。
			while (!httpRequest(bpIn, outQueue));
		} catch (IOException e1) {
			LOGGER.error("log4jException:", e1);
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			LOGGER.error("log4jException:", e1);
			e1.printStackTrace();
		}
		
		
		try {
			// 每成功请求一次，休眠。
			Thread.sleep(arProperties.getHttpThreadsSleepMs());
		} catch (InterruptedException e) {
			LOGGER.error("log4jException:", e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 请求百度地图API。
	 * @param bpIn 从输入队列读取的数据。
	 * @return 是否请求成功。
	 * @throws IOException Jsoup的get()方法异常
	 * @throws InterruptedException 把结果put()到输出队列的中断异常。
	 */
	private boolean httpRequest(BpObject<String> bpIn, LinkedBlockingQueue<BpObject<String>> outQueue) throws IOException, InterruptedException {
		
		// 得到bppc断点框架的输出队列。
//		LinkedBlockingQueue<BpObject<String>> outQueue = getContext().getOutQueue();

		// 得到需要的数据。
		String inValue = bpIn.getValue();
		
		// 百度地图API。
		String baiduUrlStringFormat = arProperties.getBaiduApiUrl();
		
		// 把输入数据分词，验证数据是否符合结构。
		String[] words = inValue.split(arProperties.getSeparator());
		if (words.length < arProperties.getAddressIndex() + 1) {
			outQueue.put(bpIn);
			LOGGER.info("该数据不符合结构：" + bpIn);
			return true;
		}

		// 得到地址信息。
		String address = words[arProperties.getAddressIndex()];
		if (address.isEmpty()) {
			outQueue.put(bpIn);
			LOGGER.info("该地址为空：" + bpIn);
			return true;
		}
		
		// 加入省市或省信息。
		address = arProperties.getAddressCity() + address;
		
		// 去除地址中的特殊字符
		address = address.replaceAll("[^0-9a-zA-Z_\\-_\u4e00-\u9fa5]", "");
		
		String key = keyList.get(new Random().nextInt(keyList.size()));
		// 合并成URL。
		String baiduUrlString = String.format(baiduUrlStringFormat, address, key);
		
		// 网络请求。
		Document doc = Jsoup.connect(baiduUrlString)
							.ignoreContentType(true)
							.timeout(3000)
							.get();
		
		// 得到返回的数据。
		String jsonString = doc.body().text();
		
		// 返回的数据为空，请求失败，返回false，继续请求。
		if (jsonString == null || jsonString.isEmpty()) {
			LOGGER.info("Http JSON is empty. Please try later.");
			return false;
		}
		
		// 解析JSON。
		BaiduJSON baiduJSON = BaiduJSON.parse(jsonString);
		
		// 得到经纬度为空的默认值。
		String nullString = arProperties.getCoordinatesNullString();
		
		// 根据JSON的状态码进行处理。
		switch (baiduJSON.getStatus()) {
		// 成功，放到输出队列。
			case SUCCESS:
				if (baiduJSON.getPrecise() == 1 || baiduJSON.getConfidence() >= arProperties.getBaiduConfidence()) {
					// 精确打点，不修改经纬度的值。
				} else {
					// 模糊打点，设置经纬度的值为null。
					baiduJSON.setLat(nullString);
					baiduJSON.setLng(nullString);
				}
				BpObject<String> bpOut = new BpObject<String>(bpIn.getRow(), combine(inValue, baiduJSON));
				outQueue.put(bpOut);
				count++;
				if (count > Max) {
					System.exit(0);
				}
				break;
			// 成功但没有数据，放到输出队列。
			case SUCCESS_BUT_NO_RESULT:
			// 参数错误，可能是地址为空，很少，放到输出队列。
			case PARAMETER_ERROR:
				baiduJSON.setLat(nullString);
				baiduJSON.setLng(nullString);
				BpObject<String> bpOut2 = new BpObject<String>(bpIn.getRow(), combine(inValue, baiduJSON));
				outQueue.put(bpOut2);
				break;
			// 超限。
			case OUT_OF_QUOTA_MINUTE:
				LOGGER.info("key:" + key + "的每分钟配额超限，请一分钟后重试。");
				Thread.sleep(60000);
				return false;
			// 超限。
			case OUT_OF_QUOTA_DAY:
				LOGGER.info("key:" + key + "的天配额超限，限制访问，请隔天后再继续。");
				keyList.remove(key);
				if (keyList.isEmpty()) {
					System.exit(302);
				}
				return false;
			// 其他情况。
			default:
				LOGGER.info("key:" + key + "; " + jsonString + ", 请查明问题并重试。");
				keyList.remove(key);
				if (keyList.isEmpty()) {
					System.exit(-1);
				}
				return false;
		}
		LOGGER.trace("status=" + baiduJSON.getStatus() + ", row=" + bpIn.getRow() + ", address=" + address + "\t" + baiduJSON.getLng() + "," + baiduJSON.getLat());
		return true;
	}
	
	private String combine(String inValue, BaiduJSON baiduJSON) {
		StringBuilder sb = new StringBuilder();
		String separator = arProperties.getSeparator();
		sb.append(inValue).append(separator)
		  .append(baiduJSON.getLng()).append(separator)
		  .append(baiduJSON.getLat());

		return sb.toString();
	}
	
	@Override
	protected String transform(String inValue) {
		return null;
	}
}
