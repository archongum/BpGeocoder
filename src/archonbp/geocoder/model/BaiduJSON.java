package archonbp.geocoder.model;

import org.json.JSONObject;

/**
 * 解析百度地图返回的JSON模型类。 
 * @author AlexGumHub 20160825
 *
 */
public class BaiduJSON {
	
	private static String STATUS_KEY = "status";
	private static String LNG_KEY = "lng";
	private static String LAT_KEY = "lat";
	private static String PRECISE_KEY = "precise";
	private static String CONFIDENCE_KEY = "confidence";
	private static String RESULT_KEY = "result";
	private static String LOCATION_KEY = "location";
	
	
	private int status;
	private String lng;
	private String lat;
	private int precise;
	private int confidence;
	
	
	public static BaiduJSON parse(String jsonString) {
		BaiduJSON baiduJSON = new BaiduJSON();
		

		JSONObject object = new JSONObject(jsonString);
		baiduJSON.setStatus(object.optInt(STATUS_KEY, -1));
		JSONObject resultObject = object.optJSONObject(RESULT_KEY);
		if (resultObject == null) {
			baiduJSON.setPrecise(-1);
			baiduJSON.setConfidence(-1);
		} else {
			baiduJSON.setPrecise(resultObject.optInt(PRECISE_KEY, -1));
			baiduJSON.setConfidence(resultObject.optInt(CONFIDENCE_KEY, -1));
			
			JSONObject locationObject = resultObject.optJSONObject(LOCATION_KEY);
			if (locationObject != null) {
				baiduJSON.setLng(locationObject.optString(LNG_KEY));
				baiduJSON.setLat(locationObject.optString(LAT_KEY));
			} else {
				baiduJSON.setLng("");
				baiduJSON.setLat("");
			}
		}
		
		
		
		return baiduJSON;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public int getPrecise() {
		return precise;
	}
	public void setPrecise(int precise) {
		this.precise = precise;
	}
	public int getConfidence() {
		return confidence;
	}
	public void setConfidence(int confidence) {
		this.confidence = confidence;
	}
}
