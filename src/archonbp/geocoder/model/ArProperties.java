package archonbp.geocoder.model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * 配置文件的模型类。 
 * @author AlexGumHub 20160825
 *
 */
public class ArProperties {
	/**
	 * 分割输入源数据之后，地址信息所在的下标。
	 */
	private int addressIndex;

	/**
	 * 城市所在的省市。
	 */
	private String addressCity;
	
	/**
	 * 输入文件的字符编码。
	 */
	private String charset;
	
	/**
	 * 输入目录。
	 */
	private String inDir;
	
	/**
	 * 输出目录。
	 */
	private String outDir;
	
	/**
	 * 临时目录。
	 */
	private String tmpDir;
	
	/**
	 * 数据分隔符。
	 */
	private String separator;
	
	/**
	 * 百度地图的API。
	 */
	private String baiduApiUrl;
	
	/**
	 * 百度地图API里面的key。
	 */
	private String baiduKey;
	
	/**
	 * 百度地图API里面的打点精度。
	 */
	private int baiduConfidence;
	
	/**
	 * 网络请求线程数目。
	 */
	private int httpThreadsSum;
	
	/**
	 * 网络请求线程，每请求一次睡眠时间。
	 */	
	private int httpThreadsSleepMs;
	
	/**
	 * 没有经纬度时的默认值。
	 */
	private String coordinatesNullString;

	/**
	 * 测试版。
	 */
	private boolean testVersion;

	public ArProperties(String configFile) throws FileNotFoundException, IOException {
		Properties p = new Properties();
		p.load(new FileInputStream(configFile));
		this.addressIndex = Integer.parseInt(p.getProperty("address_index").trim());
		this.addressCity = p.getProperty("address_city");
		this.charset = p.getProperty("charset").trim();
		this.inDir = p.getProperty("in_dir").trim();
		this.outDir = p.getProperty("out_dir").trim();
		this.tmpDir = p.getProperty("tmp_dir").trim();
		this.separator = p.getProperty("separator");
		this.baiduApiUrl = p.getProperty("baidu_api_url").trim();
		this.baiduKey = p.getProperty("baidu_key").trim();
		this.baiduConfidence = Integer.parseInt(p.getProperty("baidu_confidence"));
		this.httpThreadsSum = Integer.parseInt(p.getProperty("http_threads_sum").trim());
		this.httpThreadsSleepMs = Integer.parseInt(p.getProperty("http_threads_sum").trim());
		this.setCoordinatesNullString(p.getProperty("coordinates_null_string").trim());
		this.testVersion = Boolean.parseBoolean(p.getProperty("charset").trim());
	}

	public int getAddressIndex() {
		return addressIndex;
	}

	public void setAddressIndex(int addressIndex) {
		this.addressIndex = addressIndex;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getInDir() {
		return inDir;
	}

	public void setInDir(String inDir) {
		this.inDir = inDir;
	}

	public String getOutDir() {
		return outDir;
	}

	public void setOutDir(String outDir) {
		this.outDir = outDir;
	}

	public String getTmpDir() {
		return tmpDir;
	}

	public void setTmpDir(String tmpDir) {
		this.tmpDir = tmpDir;
	}

	public String getSeparator() {
		return separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public String getBaiduApiUrl() {
		return baiduApiUrl;
	}

	public void setBaiduApiUrl(String baiduApiUrl) {
		this.baiduApiUrl = baiduApiUrl;
	}

	public String getBaiduKey() {
		return baiduKey;
	}

	public void setBaiduKey(String baiduKey) {
		this.baiduKey = baiduKey;
	}

	public int getHttpThreadsSum() {
		return httpThreadsSum;
	}

	public void setHttpThreadsSum(int httpThreadsSum) {
		this.httpThreadsSum = httpThreadsSum;
	}

	public boolean isTestVersion() {
		return testVersion;
	}

	public void setTestVersion(boolean testVersion) {
		this.testVersion = testVersion;
	}

	public int getBaiduConfidence() {
		return baiduConfidence;
	}

	public void setBaiduConfidence(int baiduConfidence) {
		this.baiduConfidence = baiduConfidence;
	}

	@Override
	public String toString() {
		return "ArProperties [addressIndex=" + addressIndex + ", charset=" + charset + ", inDir=" + inDir + ", outDir="
				+ outDir + ", tmpDir=" + tmpDir + ", separator=" + separator + ", baiduApiUrl=" + baiduApiUrl
				+ ", baiduKey=" + baiduKey + ", baiduConfidence=" + baiduConfidence + ", httpThreadsSum="
				+ httpThreadsSum + ", testVersion=" + testVersion + "]";
	}

	public int getHttpThreadsSleepMs() {
		return httpThreadsSleepMs;
	}

	public void setHttpThreadsSleepMs(int httpThreadsSleepMs) {
		this.httpThreadsSleepMs = httpThreadsSleepMs;
	}

	public String getAddressCity() {
		return addressCity;
	}

	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}

	public String getCoordinatesNullString() {
		return coordinatesNullString;
	}

	public void setCoordinatesNullString(String coordinatesNullString) {
		this.coordinatesNullString = coordinatesNullString;
	}
	
	
}
