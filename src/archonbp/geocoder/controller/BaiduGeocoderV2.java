package archonbp.geocoder.controller;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;

import archon.bppc.consumer.BpConsumer;
import archon.bppc.consumer.MidConsumer;
import archon.bppc.controller.BpController;
import archon.bppc.model.BpObject;
import archon.bppc.model.BpProperties;
import archon.bppc.pruducer.BpProducer;
import archon.bppc.pruducer.StringBpProducer;
import archonbp.geocoder.consumer.BaiduGeocoderV2Consumer;
import archonbp.geocoder.model.ArProperties;

/**
 * 主控类。
 * Version 1.1: 优化：一分钟配额超限不会退出，而是sleep一分钟之后继续执行。
 * Version 1.2: 需求更新：无经纬度数据的值，不写出。
 * @author AlexGumHub 20160825
 *
 */
public class BaiduGeocoderV2 {
	
	public static void main(String[] args) {
		baiduGeocoding();
	}
	
	public static void test() {
		
	}
	
	public static void baiduGeocoding() {
		ArProperties arProperties = null;
		try {
			arProperties = new ArProperties("config/config.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (arProperties == null) {
			System.err.println("配置文件不存在，请检查。");
		}
		final String nullString = arProperties.getCoordinatesNullString();
		
		BpProperties bpProperties = BpProperties.getDefaultBpProperties();
		bpProperties.setInDirPath(arProperties.getInDir());
		bpProperties.setOutDirPath(arProperties.getOutDir());
		bpProperties.setTmpDirPath(arProperties.getTmpDir());
		bpProperties.setCharsetName(arProperties.getCharset());
		bpProperties.setMidConsumerThreadCount(arProperties.getHttpThreadsSum());
		bpProperties.setMidConsumerPollTimeoutMs(arProperties.getHttpThreadsSleepMs());
		
		BpProducer<String> bpProducer = new StringBpProducer();
		MidConsumer<String, String> midConsumer = new BaiduGeocoderV2Consumer(arProperties);
		BpConsumer<String> bpConsumer = new BpConsumer<String>() {
			
			@Override
			protected void handle(BpObject<String> bpOut, BufferedWriter outFileBw) {
				if (bpOut.getValue().contains(nullString)) return;
				super.handle(bpOut, outFileBw);
			}

			@Override
			protected String formatOUT(String out) {
				return out;
			}
		};
		
		BpController<String, String> bpController = new BpController<String, String>(bpProperties, bpProducer, midConsumer, bpConsumer);
		bpController.run();
	}
}
